# w21a
<p align="center">
  <img src="https://rawcdn.githack.com/1DavidCarbon/Soapbox_Launcher_Installer/master/Images/Unofficial Installer Banner.png" />
</p>

# Installer_NFSW 
[![Build Status](https://github.com/1DavidCarbon/GameLauncher_NFSW/workflows/Build%20Status/badge.svg)](https://github.com/1DavidCarbon/Soapbox_Launcher_Installer/releases/latest) 
[![Total Downloads](https://img.shields.io/github/downloads/1DavidCarbon/Soapbox_Launcher_Installer/total.svg?colorB=informational&label=downloads&logo=GitHub&style=flat)](https://github.com/1DavidCarbon/Soapbox_Launcher_Installer/releases/latest) 
[![Stable Release](https://img.shields.io/github/release/1DavidCarbon/Soapbox_Launcher_Installer.svg?label=stable%20release&logo=downloads&style=flat)](https://github.com/1DavidCarbon/Soapbox_Launcher_Installer/releases/latest) 
[![Preview Release](https://img.shields.io/github/release-pre/1DavidCarbon/Soapbox_Launcher_Installer.svg?label=pre-release&style=flat)](https://github.com/1DavidCarbon/Soapbox_Launcher_Installer/releases/latest) 
[![License](https://img.shields.io/github/license/1DavidCarbon/Soapbox_Launcher_Installer.svg?style=flat)](https://github.com/1DavidCarbon/Soapbox_Launcher_Installer/blob/master/LICENSE)
[![Discord](https://img.shields.io/discord/311140210018615310.svg?label=&logo=discord&logoColor=ffffff&color=7389D8&labelColor=6A7EC2)](https://discord.gg/TUsXvVp)

*Want to see all of the old installer patch notes?*

Now you can! Visit the [patch notes txt file][installer patch notes].

[installer patch notes]: https://1davidcarbon.gitlab.io/soapbox-installer-releases-download/Update%20History.txt

## Included in Installer Version
- .NET Framework 4.6 (Web Installer)
- Official Game Launcher
- Tools
  - Vinyl Manager
- [X] Windows Firewall Exception
- [x] Custom Directory

## Requirement to install Game Launcher
- [X] .NET Framework 4.6
- [X] Windows Vista SP2 or Newer
- [x] Admin Rights

### Screenshots
Start & Finish Prompt             |  Default Directory
:-------------------------:|:-------------------------:
![Language Prompt](https://rawcdn.githack.com/1DavidCarbon/Soapbox_Launcher_Installer/master/Images/Soapbox%20Installer%20(Lang).PNG) | ![Default Directory](https://rawcdn.githack.com/1DavidCarbon/Soapbox_Launcher_Installer/master/Images/Soapbox%20Installer%20(Dir).PNG)
![Finished Installation Prompt](https://rawcdn.githack.com/1DavidCarbon/Soapbox_Launcher_Installer/master/Images/Soapbox%20Installer%20(Launch).PNG) |

<p align="center">
  <img src="https://rawcdn.githack.com/1DavidCarbon/Soapbox_Launcher_Installer/master/Images/Soapbox%20Installer%20(Firewall).PNG" alt="Windows Firewall Exceptions" />
</p>

# GameLauncher_NFSW [![Build Status](https://github.com/1DavidCarbon/GameLauncher_NFSW/workflows/Build%20Status/badge.svg)](https://github.com/1DavidCarbon/GameLauncher_NFSW/actions)
A Rewrite of GameLauncher taken from Need For Speed: World

## Todo
- [X] Downloader?
- [X] Validate files
- [X] Complete UI
- [X] Original fonts

### Screenshot
![](https://raw.githubusercontent.com/SoapboxRaceWorld/GameLauncher_NFSW/interface_v3/screenshot.png)

(Source: [Soapbox Race World](https://github.com/SoapboxRaceWorld/GameLauncher_NFSW))